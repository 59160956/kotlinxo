import java.util.*

fun main(args: Array<String>){
    var gameStart = game()
    gameStart.game()
    gameStart.play()
}

class game{
    val max = Scanner(System.`in`)
    var Table = Board()
    var X = Table.X
    var O = Table.O
    var Turn = 'X'
    fun game(){
        X.Player('X')
        O.Player('O')
    }
    fun play(){
        showWelcome()
        for (a in 1..999){
            for (i in 1..9){
                showTable()
                showTurn()
                input()
                if (showWin()){
                    showTable()
                    break
                }
                if( i == 9){
                    println("Draw !!")
                    X.draw += 1
                    O.draw += 1
                    break
                }
            }
            println("restart game? (Y/N) :")
            var re = max.next()
            if(re.equals("N")){
                X.getPlayer()
                println()
                O.getPlayer()
                showBye()
                break
            }
            Table.Table = arrayOf(arrayOf("-", "-", "-"), arrayOf("-", "-", "-"), arrayOf("-", "-", "-"))
        }

    }
    fun showWelcome(){
        println("Welcome To XO Game")
    }
    fun showTable(){
        var Table = Table.Table
        println("  1 2 3")
        for(i in 0..2){
            print("${(i+1)} ")
            for (j in 0..2){
                print(Table[i][j]+" ")
            }
            println()
        }
    }
    fun showTurn(){
        println("${Turn} Turn")
    }
    fun input(){

        println("Please input Row Col :")
        var Row = max.nextInt()-1
        var Col = max.nextInt()-1
        for (i in 1..999){
            if(Table.Table[Row][Col].equals("-")){
                break
            }else{
                println("try to input")
                println("Please input Row Col :")
                 Row = max.nextInt()-1
                 Col = max.nextInt()-1
            }
        }
        if(Turn.equals('X')){
            Table.Board(X,Row,Col)
            Turn = 'O'
        }else{
            Table.Board(O,Row,Col)
            Turn = 'X'
        }
    }
    fun showWin(): Boolean {
        var T = Table.Table
        for (i in 0..2){
            if(T[i][0].equals(T[i][1])&&T[i][1].equals(T[i][2])){
                if(!T[i][0].equals("-")&&!T[i][1].equals("-")&&!T[i][2].equals("-")){
                    println(T[i][0] + "win !!")
                    whoWin(T[i][0].toString())
                    return true
                }
            }
            if(T[0][i].equals(T[1][i])&&T[1][i].equals(T[2][i])){
                if(!T[0][i].equals("-")&&!T[1][i].equals("-")&&!T[2][i].equals("-")){
                    println(T[0][i] + " win !!")
                    whoWin(T[0][i].toString())
                    return true
                }
            }
        }
        if(T[0][0].equals(T[1][1]) && T[1][1].equals(T[2][2])) {
            if(!T[0][0].equals("-") && !T[1][1].equals("-") && !T[2][2].equals("-")) {
                println(T[0][0] + " win !!")
                whoWin(T[0][0].toString())
                return true;
            }
        }
        if(T[0][2].equals(T[1][1]) && T[1][1].equals(T[2][0])) {
            if(!T[0][2].equals("-") && !T[1][1].equals("-") && !T[2][0].equals("-")) {
                println(T[0][2] + " win !!")
                whoWin(T[0][2].toString())
                return true;
            }
        }
                return false

    }
    fun showBye(){
        println("Thanks for Playing")
        println("Bye")
    }
    fun whoWin(P:String){
        if(P.equals("X")){
            X.win += 1
            O.lost +=1
        }else{
            X.lost += 1
            O.win += 1
        }
    }
}
class Player{
    var name: Char = 'X'
    var win = 0
    var draw = 0
    var lost = 0
    fun Player(name: Char){
        this.name = name
    }
    fun getPlayer(){
        println("Player : "+this.name)
        println("Win : "+this.win)
        println("Draw : "+this.draw)
        println("Lost : "+this.lost)
    }

}class Board {
    var X = Player()
    var O = Player()
    var Table = arrayOf(arrayOf("-", "-", "-"), arrayOf("-", "-", "-"), arrayOf("-", "-", "-"))
    fun Board(P:Player,Row:Int,Col:Int){
        Table[Row][Col] = P.name.toString()
    }
}